//app.js Angular.js modules
(function () {
  'use strict';
  var app = angular.module('app', ['ngRoute', 'infinite-scroll']);
  angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250);

  //Define Routing for app
  app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.when('/home', {
      templateUrl: 'templates/landing.html',
      controller: 'RegistrationController'
    }).when('/terms', {
      templateUrl: 'templates/terms.html'
    }).when('/policy', {
      templateUrl: 'templates/policy.html'
    }).when('/vote/:userID', {
      templateUrl: 'templates/vote.html',
      controller: 'VoteController'
    }).when('/thankyou/:userID', {
      templateUrl: 'templates/thankyou-modal.html',
      controller: 'ThankyouController'
    }).otherwise({
      redirectTo: '/home'
    });

    // $locationProvider.html5Mode(true);
  }]);

  //DatePicker Directives
  var FromEndDate = new Date();

  app.directive('datetimez', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attrs, ngModelCtrl) {
        element.datetimepicker({
          dateFormat: 'dd/MM/yyyy',
          language: 'en',
          pickTime: false,
          autoclose: true,
          endDate: FromEndDate          // set a maximum date
        }).on('changeDate', function (e) {
          ngModelCtrl.$setViewValue(e.date);
          scope.$apply();
        });
      }
    };
  });

  app.controller("MainController", ['$scope', '$http', function ($scope, $http) {
    $scope.api_base_url = api_base_url;
    $scope.domain_name = domain_name;
    $scope.img_base_url = img_base_url;
  }]);

  /* ==== Registration ===*/
  app.controller("RegistrationController", ['$scope', '$http', function ($scope, $http) {
    $scope.heightMom = function () {
      var momMeasurements = $('#momMeasur').val();
      var measurements = $scope.heightmom+momMeasurements;
      $('#measurements').val(measurements);
    };

    $scope.heightKid = function () {
      var kidMeasurements = $('#kidMeasur').val();
      var measurements = $scope.heightkid+kidMeasurements;
      $('#child-measurements').val(measurements);
    };

    // $scope.selection = [];
    $scope.selection = '';

    $scope.talentsSelection = function (talentsName) {
      $('#talentsError').fadeOut('slow');
      if($scope.selection == '') {
        $scope.selection = talentsName;
      } else {
        $scope.selection = $scope.selection+','+talentsName;
      }
      $('#child-talents').val($scope.selection);
    };

    $scope.othertalentsSelection = function () {
      var otherTalents = $('#regiForm-other-talent').val();
      if($scope.selection == '') {
        $scope.selection = otherTalents;
      } else {
        $scope.selection = $scope.selection+','+otherTalents;
      }
      $('#child-talents').val($scope.selection);
    };

    $scope.jsonstr = JSON.stringify($scope.selection);
  }]);
  /* ==== /Registration ===*/

  // Thanku Controller
  app.controller("ThankyouController", ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
    var userID = $routeParams.userID;

    //GET user data
    $scope.getKidData = function () {
      var url = api_base_url + 'api/campaign/' + userID + '.json';
      // console.log(url);
      $http({
        method: 'GET',
        url: url
      }).then(function (data) {
        $scope.kidData = data['data'];
        $scope.userID = userID;
        // console.log($scope.kidData);
      }, function (data) {
        console.log("error");
      });
    };

    $scope.shareOnFB = function (userid,name,picurl) {
      var url = domain_name+'#vote/'+userid;
      var image = img_base_url+picurl;
      FB.ui({
        method: 'share_open_graph',
        action_type: 'og.shares',
        action_properties: JSON.stringify({
          object : {
            'og:url': url,
            'og:title': name,
            // 'og:description': 'Rashmi Uppal',
            'og:og:image:width': '1200',
            'og:image:height': '600',
            'og:image': image
          }
        })
      });
    };

    $scope.getKidData();
  }]);

  // Voting Controller
  app.controller("VoteController", ['$scope', '$http', '$routeParams', '$rootScope', function ($scope, $http, $routeParams, $rootScope) {
    var userID = $routeParams.userID;

    //GET user data
    $scope.getKidData = function (email) {
      // if(email != '') {
      //   // load FB scripts, then call facebookInit
      //   $rootScope.$emit("CallParentLoadFB", {});
      // }
      $scope.loadFB($scope.facebookInit);

      var url = api_base_url + 'api/campaign/' + userID + '.json';
      $http({
        method: 'GET',
        url: url
      }).then(function (data) {
        $scope.kidData = data['data'];
        // console.log($scope.kidData);
      }, function (data) {
        console.log("error");
      });
    };

    //Submit Vote
    $scope.submitVote = function (email, userid) {
      $scope.fbLogin(userid);
    };

    //Facbook Retrive email id
    $scope.facebookInit = function () {
      FB.init({appId: '690916471032836', cookie: true, xfbml: true, oauth: true});
    };

    $scope.fbLogin = function (userid) {
      FB.login(function(response) {
        // handle the response
        if (response.status === 'connected') {
          $scope.testAPI(userid);
        } else {
          alert('Please log into this app.');
        }
      }, {scope: 'public_profile,email'});
    };

    $scope.testAPI = function (userid) {
      FB.api('/me?fields=name,email', function(response) {
        var email = $scope.kidData.userEmail = response.email;
        // console.log($scope.kidData.userEmail);

        var url = api_base_url + 'api/campaign.json?email=' + email + '&userId=' + userid;
        $http({
          method: 'GET',
          url: url
        }).then(function (data) {
          $scope.voteData = data['data'];
          if ($scope.voteData == "Success!") {
            $scope.voteResponse = "Thank you for your vote. Spread the word and show your support to this cutie by getting him/her more votes.";
          } else {
            $scope.voteResponse = $scope.voteData;
          }
          $scope.getKidData(email);
          $('.contactRepsonse').fadeIn('slow');
        }, function (data) {
          console.log("error");
        });
      });
    };

    // loads FB script asynchronously
    $scope.loadFB = function (cb) {
      var script = document.createElement('script');
      script.async = true;
      script.src = '//connect.facebook.net/en_US/all.js';
      script.onload = cb;
      document.head.appendChild(script);
    };

    $rootScope.$on("CallParentLoadFB", function () {
      $scope.loadFB($scope.facebookInit);
    });

    $scope.getKidData();
    // $scope.loadFB($scope.facebookInit);
  }]);
})();
